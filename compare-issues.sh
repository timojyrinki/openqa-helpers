#!/bin/sh
# Copyright 2024 Timo Jyrinki <tjyrinki@suse.com>
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Usage:
#
# For a failed openQA maintenace test that follows a passing one, go to the Investigation
# tab and copy-paste the (full) contents of diff_to_last_good field to a file
# "a.txt" in the current directory.
#
# Then run this script. It will mangle all the _TEST_ISSUES parameters so that 
# real, unique, added issues are found, and print them.
#
# Examples:
# ./compare_issues.sh # Prints all the new maintenance updates being tested
# ./compare_issues.sh -w # Same as above, but also opens them in browser

if ! command -v wdiff &> /dev/null ; then
    echo "Please install wdiff"
    exit 1
fi

echo
if [ "$1" == "-w" ] ; then
    echo "The following new maintenance updates will be opened in browser:"
else
    echo "The following are the new maintenance updates:"
fi
echo

tmp1=$(mktemp)
tmp2=$(mktemp)

for i in BASE_TEST_ISSUES DESKTOP_TEST_ISSUES LTSS_TEST_ISSUES PCM_TEST_ISSUES OS_TEST_ISSUES PHUB_TEST_ISSUES PYTHON3_TEST_ISSUES SCRIPT_TEST_ISSUES SDK_TEST_ISSUESTEST_TEST_ISSUES SERVERAPP_TEST_ISSUES WE_TEST_ISSUES ; do

    cat a.txt | grep "$i" | head -n 1 | tr ',' '\n' | sed 's/.*".*://' | sed 's/"//g' | sort > "$tmp1"
    cat a.txt | grep "$i" | tail -n 1 | tr ',' '\n' | sed 's/.*".*://' | sed 's/"//g' | sort > "$tmp2"

    issues="$(wdiff -1 -n -w $'\033[30;41m' -x $'\033[0m' -y $'\033[30;42mhttps://smelt.suse.de/incident/' -z $'\033[0m' "$tmp1" "$tmp2" | grep smelt | sort | uniq)"
    echo "$issues" | sed '/^[[:space:]]*$/d'

    for url in $issues; do
        url=$(echo $url | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g")
        if [ "$1" == "-w" ] ; then xdg-open $url ; fi
    done
done

rm -f "$tmp1" "$tmp2"

if [ ! "$1" == "-w" ] ; then
    echo
    echo "If you want to open those in a browser, run the command again with the '-w' switch now."
fi

echo
